﻿//============================================================
    // Student Number : S10192609
    // Student Name : Lim Junquan Ryan
    // Module Group : P02
//============================================================

using System;
using System.Collections.Generic;
using System.IO;

namespace Assignment2
{
    class Program
    {
        // Global variable for screening number
        static int screeningNo = 1001;
        static void Main(string[] args)
        {
            //variables for menu option and lists
            string option;
            List<Movie> movieList = new List<Movie>();
            List<Cinema> cinemaList = new List<Cinema>();
            List<Screening> screeningList = new List<Screening>();

            //calling initialise methods
            InitMovieCinemaList(movieList, cinemaList);
            InitScreeningList(movieList, cinemaList, screeningList);

            //main code loop which calls Menu method and option related methods
            do
            {
                Menu();
                option = Console.ReadLine();
                Console.WriteLine();
                if (option == "0") { Console.WriteLine("Thank you for using the Movie Ticketing System"); }
                else if (option == "1") { DisplayMovieDetails(movieList); }
                else if (option == "2") { DisplayMovieScreenings(movieList, screeningList); }
                else if (option == "3") { AddMovieScreening(movieList, cinemaList, screeningList); }
                else if (option == "4") { DeleteMovieScreening(movieList, screeningList); }
                else if (option == "5") { DisplayAvailableSeats(screeningList); }
                else { Console.WriteLine("Invalid Selection. Please Select Again."); }
                Console.WriteLine();
            } while (option != "0");
        }
        //Menu method that displays main menu
        static void Menu()
        {
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("------------ Movie Ticketing System ------------");
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine();
            Console.WriteLine("[1] Display Movies                              ");
            Console.WriteLine("[2] Display Movie Screenings                    ");
            Console.WriteLine("[3] Add a Movie Screening                       ");
            Console.WriteLine("[4] Delete a Movie Screening                    ");
            Console.WriteLine("[5] Display Available Seats of Screening Session");
            Console.WriteLine("[0] Exit                                        ");
            Console.WriteLine();
            Console.Write("Select an option: ");
        }
        //Initialise method to initialise movieList and cinemaList
        static void InitMovieCinemaList(List<Movie> mList, List<Cinema> cList)
        {
            //Reads Movie.csv into movieData
            string[] movieData = File.ReadAllLines("Movie.csv");
            for (int i = 1; i < movieData.Length; i++)
            {
                //Splits each line of Movie.csv into Movie attributes
                string[] data = movieData[i].Split(",", StringSplitOptions.RemoveEmptyEntries);
                //If line is empty of Movie attributes, skip
                if (data.Length == 0) { continue; }
                //Further split OpeningDate and Genres attributes into arrays for ease of usage
                string[] openingDate = data[4].Split("/");
                string[] genreList = data[2].Split("/");
                //Adds new Movie object into movieList with Movie attributes that are saved above
                mList.Add(new Movie(data[0], Convert.ToInt32(data[1]), data[3], new DateTime(Convert.ToInt32(openingDate[2]), Convert.ToInt32(openingDate[1]), Convert.ToInt32(openingDate[0])), new List<string>(genreList)));
            }
            //Reads Cinema.csv into cinemaData
            string[] cinemaData = File.ReadAllLines("Cinema.csv");
            for (int i = 1; i < cinemaData.Length; i++)
            {
                //Splits each line of Cinema.csv into Cinema attributes
                string[] data = cinemaData[i].Split(",", StringSplitOptions.RemoveEmptyEntries);
                //If line is empty of Movie attributes, skip
                if (data.Length == 0) { continue; }
                //Adds new Cinema object into cinemaList with Cinema attributes that are saved above
                cList.Add(new Cinema(data[0], Convert.ToInt32(data[1]), Convert.ToInt32(data[2])));
            }
        }
        //Initialise method to initialise screeningList with data from movieList and cinemaList
        static void InitScreeningList(List<Movie> mList, List<Cinema> cList, List<Screening> sList)
        {
            //Reads Screening.csv into screeningData
            string[] screeningData = File.ReadAllLines("Screening.csv");
            for (int i = 1; i < screeningData.Length; i++)
            {
                //Splits each line of Screening.csv into Screening attributes
                string[] data = screeningData[i].Split(",", StringSplitOptions.RemoveEmptyEntries);
                //If line is empty of Screening attributes, skip
                if (data.Length == 0) { continue; }
                //Further split ScreeningDateTime attribute into array for ease of usage
                string[] dateTime = data[0].Split(new string[] { "/", " ", ":", "PM", "," }, StringSplitOptions.RemoveEmptyEntries);
                //Creates new Screening object with Screening attributes that are saved above
                Screening tempScreening = new Screening(screeningNo, new DateTime(Convert.ToInt32(dateTime[2]), Convert.ToInt32(dateTime[1]), Convert.ToInt32(dateTime[0]), Convert.ToInt32(dateTime[3]) + 12, Convert.ToInt32(dateTime[4]), 0), data[1], cList.Find(Cinema => Cinema.Name == data[2] && Cinema.HallNo == Convert.ToInt32(data[3])), mList.Find(Movie => Movie.Title == data[4]));
                //Add tempScreening to screeningList
                sList.Add(tempScreening);
                //Initialise SeatsRemaining attribute with value from Screening.csv
                sList[i - 1].SeatsRemaining = Convert.ToInt32(data[5]);
                //Add tempScreening to related Movie object's ScreeningList
                mList.Find(Movie => Movie.Title == data[4]).ScreeningList.Add(tempScreening);
                //Adds 1 to Screening Number, which is a running number
                screeningNo += 1;
            }
        }
        //Display Method to display details about all movies in movieList
        static void DisplayMovieDetails(List<Movie> mList) 
        {
            //Display header with Movie attribute names
            Console.WriteLine("{0,-25} {1,-18} {2,-25} {3,-18} {4}", "Title", "Duration (Mins)", "Genre", "Classification", "Opening Date");
            Console.WriteLine("{0,-25} {1,-18} {2,-25} {3,-18} {4}", "-----", "---------------", "-----", "--------------", "------------");
            //Display data using a foreach loop
            foreach (Movie m in mList) 
            {
                Console.WriteLine("{0,-25} {1,-18} {2,-25} {3,-18} {4}", m.Title, m.Duration, string.Join("/", m.GenreList), m.Classification, m.OpeningDate.ToString("dd/MM/yyyy"));
            } 
        }
        //Display and Prompt Method for Movie Title
        static int DisplayMoviesAndPrompt(List<Movie> mList)
        {
            //Display List of Movie Titles
            Console.WriteLine("------------------");
            Console.WriteLine("----- Movies -----");
            Console.WriteLine("------------------");
            Console.WriteLine();
            for (int i = 0; i < mList.Count; i++) { Console.WriteLine("[{0}] {1}", i + 1, mList[i].Title); }
            //Initialise selected which stores the option of the selected movie
            int selected = -2;
            //do while loop with try catch to prompt user for selected movie
            do
            {
                try
                {
                    Console.Write("\nSelect a Movie (OR '0' to Exit back to Menu): ");
                    selected = Convert.ToInt32(Console.ReadLine()) - 1;
                    //exits to main menu
                    if (selected == -1) { return -1; }
                    else if (selected < 0 || selected >= mList.Count) { Console.WriteLine("Out of Range"); }
                }
                catch (Exception ex) { Console.WriteLine(ex.Message); }
            } while (selected < 0 || selected >= mList.Count);
            Console.WriteLine();
            return selected;
        }
        //Display method to display details of movie screenings
        static void DisplayMovieScreenings(List<Movie> mList, List<Screening> sList)
        {
            //Calls DisplayMoviesAndPrompt method and stores returned value
            int selected = DisplayMoviesAndPrompt(mList);
            //exits to main menu
            if (selected == -1) { return; }
            //Checks if the selected movie has screenings and prints it out if it does
            if (mList[selected].ScreeningList.Count == 0) { Console.WriteLine("No Screening session for this movie currently available"); }
            else
            {
                Console.WriteLine("{0,-20} {1,-25} {2,-25} {3,-20} {4,-25}", "Screening Number", "Screening DateTime", "Cinema Hall", "Screening Type", "Movie Title");
                Console.WriteLine("{0,-20} {1,-25} {2,-25} {3,-20} {4,-25}", "----------------", "------------------", "-----------", "--------------", "-----------");
                foreach (Screening s in mList[selected].ScreeningList) { Console.WriteLine("{0,-20} {1,-25} {2,-25} {3,-20} {4,-25}", s.ScreeningNo, s.ScreeningDateTime.ToString("dd/MM/yyyy hh:mm:sstt"), string.Format("{0} Hall {1}", s.Cinema.Name, s.Cinema.HallNo), s.ScreeningType, s.Movie.Title); }
            }
        }
        //Addition Method to add new movie screening method to screeningList and movie object's screening list
        static void AddMovieScreening(List<Movie> mList, List<Cinema> cList, List<Screening> sList)
        {
            //Calls DisplayMoviesAndPrompt method and stores returned value
            int selected = DisplayMoviesAndPrompt(mList);
            //exits to main menu
            if (selected == -1) { return; }
            //Initialise screeningType variable
            string screeningType;
            //do while loop to prompt for screeningType or exit
            do
            {
                Console.Write("Enter Screening Type [2D/3D] (OR '0' to Exit back to Menu): ");
                screeningType = Console.ReadLine().ToUpper();
                //exits to main menu
                if (screeningType == "0") { return; }
                else if (screeningType != "2D" && screeningType != "3D") { Console.WriteLine("Out of Range."); }
                else { Console.WriteLine(); }
                //while condition ensures screeningType is 2D or 3D
            } while (screeningType != "2D" && screeningType != "3D");
            //Initialise screeningDateTime variable 
            DateTime screeningDateTime;
            //do while loop with try catch to prompt for screeningDateTime or exit
            do
            {
                screeningDateTime = new DateTime();
                try
                {
                    Console.Write("Enter Screening Date and Time (24h format) [DD/MM/YYYY,HH:mm] (OR '0' to Exit back to Menu): ");
                    string input = Console.ReadLine();
                    //exits to main menu
                    if (input == "0") { return; }
                    //Split prompted DateTime into parts based on the format
                    string[] data = input.Split(new char[] { '/', ',', ':' });
                    //Create new DateTime object based on the values split from prompt
                    screeningDateTime = new DateTime(Convert.ToInt32(data[2]), Convert.ToInt32(data[1]), Convert.ToInt32(data[0]), Convert.ToInt32(data[3]), Convert.ToInt32(data[4]), 0);
                    if (screeningDateTime < mList[selected].OpeningDate) { Console.WriteLine("Out of Range."); }
                    else { Console.WriteLine(); }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            } while (screeningDateTime < mList[selected].OpeningDate);

            //Display all cinema halls
            Console.WriteLine("---------------------------------");
            Console.WriteLine("---------- Cinema Halls ---------");
            Console.WriteLine("---------------------------------");
            Console.WriteLine();
            for (int i = 0; i < cList.Count; i++) { Console.WriteLine("[{0}] {1} Hall Number {2}", i + 1, cList[i].Name, cList[i].HallNo); }
            Console.WriteLine();

            //Initialise cineHall and available variables 
            int cineHall = 0;
            bool available;
            //do while loop with try catch to prompt for cineHall or exit
            do
            {
                available = false;
                try
                {
                    Console.Write("Select a Cinema and Hall (OR '0' to Exit back to Menu): ");
                    cineHall = Convert.ToInt32(Console.ReadLine()) - 1;
                    //exits to main menu
                    if (cineHall == -1) { return; }
                    //foreach loop to check through all screenings at the specified cinema hall
                    foreach (Screening s in sList.FindAll(Screening => Screening.Cinema.Name == cList[cineHall].Name && Screening.Cinema.HallNo == cList[cineHall].HallNo))
                    {
                        //checks if new screening will clash with existing screenings
                        if (s.ScreeningDateTime > screeningDateTime.AddMinutes(mList[selected].Duration + 30)|| screeningDateTime > s.ScreeningDateTime.AddMinutes(s.Movie.Duration + 30)) { available = true; }
                        else 
                        { 
                            available = false;
                            Console.WriteLine("Screening Timing at Specified Cinema Hall Unavailable.");
                            break;
                        }
                    }
                    Console.WriteLine();
                }
                catch (Exception ex) { Console.WriteLine(ex.Message); }
            } while (!(available));
            //try catch to ensure that if exception occurs, code will not crash
            try
            {
                //Create new Screening object with values obtained via user prompts
                Screening tempScreening = new Screening(screeningNo, screeningDateTime, screeningType, cList[cineHall], mList[selected]);
                //Initialise tempScreening SeatsRemaining = Cinema capacity
                tempScreening.SeatsRemaining = tempScreening.Cinema.Capacity;
                //Add tempScreening to screeningList
                sList.Add(tempScreening);
                //Add tempScreening to related Movie object's ScreeningList
                mList[selected].ScreeningList.Add(tempScreening);
                screeningNo += 1;
                Console.WriteLine("Movie Screening Session Creation Successful");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Movie Screening Session Creation Unsuccessful");
            }
        }
        //Deletion Method to delete Movie Screening
        static void DeleteMovieScreening(List<Movie> mList, List<Screening> sList)
        {
            try
            {
                //Initialise count and snList variables
                int count = 1;
                List<int> snList = new List<int> { };
                //Display header with Screening attribute names and screening number
                Console.WriteLine("S/N   {0, -20} {1, -25} {2, -25} {3, -20} {4, -25}", "Screening Number", "Screening DateTime", "Cinema Hall", "Screening Type", "Movie Title");
                Console.WriteLine("---   {0, -20} {1, -25} {2, -25} {3, -20} {4, -25}", "----------------", "------------------", "-----------", "--------------", "-----------");
                //foreach loop with findAll method to find and display all screenings that have not sold any tickets
                foreach (Screening s in sList.FindAll(Screening => Screening.SeatsRemaining == Screening.Cinema.Capacity))
                {
                    Console.WriteLine("{5, -5:[0]} {0, -20} {1, -25} {2, -25} {3, -20} {4, -25}", s.ScreeningNo, s.ScreeningDateTime.ToString("dd/MM/yyyy hh:mm:sstt"), string.Format("{0} Hall {1}", s.Cinema.Name, s.Cinema.HallNo), s.ScreeningType, s.Movie.Title, count);
                    count += 1;
                    snList.Add(s.ScreeningNo);
                }
                Console.WriteLine();
                Console.Write("Delete a Screening Session (OR '0' to Exit back to Menu): ");
                //prompt for selected Movie Screening to delete
                int selected = Convert.ToInt32(Console.ReadLine());
                //exits to main menu
                if (selected == 0) { return; }
                //Creates toBeRemoved object by searching for its screening number 
                Screening toBeRemoved = sList.Find(Screening => Screening.ScreeningNo == snList[selected - 1]);
                //removes toBeRemoved from ScreeningList 
                sList.Remove(toBeRemoved);
                //removes toBeRemoved from the associated Movie Object's ScreeningList
                mList.Find(Movie => Movie.ScreeningList.Contains(toBeRemoved)).ScreeningList.Remove(toBeRemoved);
                Console.WriteLine("Deletion of Screening Session Successful");
            }
            catch (Exception ex) { Console.WriteLine("Deletion of Screening Session Unsuccessful"); }
        }
        //Display Method to display Movie Screenings in descending order of Available seats
        static void DisplayAvailableSeats(List<Screening> sList)
        {
            //Create a temporary copy of screeningList to be sorted in descending order of Available seats
            List<Screening> tempSList = sList;
            tempSList.Sort();
            //Display Header
            Console.WriteLine("{0, -20} {1, -20} {2, -15} {3, -25} {4, -25} {5, -25}", "Screening Number", "Movie Title", "Screening Type", "Screening Date Time", "Cinema Hall", "Available Seats");
            Console.WriteLine("{0, -20} {1, -20} {2, -15} {3, -25} {4, -25} {5, -25}", "----------------", "-----------", "--------------", "-------------------", "-----------", "---------------");
            //Display Screenings in order of sorted tempSList
            for (int i = tempSList.Count - 1; i >= 0; i--)
            {
                Console.WriteLine("{0, -20} {1, -20} {2, -15} {3, -25} {4, -25} {5, -25}", tempSList[i].ScreeningNo, tempSList[i].Movie.Title, tempSList[i].ScreeningType, tempSList[i].ScreeningDateTime.ToString("dd/MM/yyyy hh:mm:sstt"), string.Format("{0} Hall {1}", tempSList[i].Cinema.Name, tempSList[i].Cinema.HallNo), tempSList[i].SeatsRemaining);
            }
        }
    }
    class Movie
    {
        public string Title { get; set; }
        public int Duration { get; set; }
        public string Classification { get; set; }
        public DateTime OpeningDate { get; set; }
        public List<string> GenreList { get; set; } = new List<string> { };
        public List<Screening> ScreeningList { get; set; } = new List<Screening> { };
        public Movie() { }
        public Movie(string t, int d, string c, DateTime o, List<string> g)
        {
            Title = t;
            Duration = d;
            Classification = c;
            OpeningDate = o;
            GenreList = g;
        }
        public void AddGenre()
        {
            Console.Write("Genre to add to this movie: ");
            GenreList.Add(Console.ReadLine());
        }
        public override string ToString() { return string.Format("Title: {0} Duration: {1} Classification: {2} Opening Date: {3} Genre List: {4} Screening List: {5}", Title, Duration, Classification, OpeningDate, string.Join("/", GenreList), string.Join("/", ScreeningList)); }
    }
    class Cinema
    {
        public string Name { get; set; }
        public int HallNo { get; set; }
        public int Capacity { get; set; }
        public Cinema() { }
        public Cinema(string n, int h, int c)
        {
            Name = n;
            HallNo = h;
            Capacity = c;
        }
        public override string ToString()
        {
            return string.Format("Name: {0} HallNo: {1} Capacity: {2}", Name, HallNo, Capacity);
        }
    }
    class Screening : IComparable<Screening>
    {
        public int ScreeningNo { get; set; }
        public DateTime ScreeningDateTime { get; set; }
        public string ScreeningType { get; set; }
        public int SeatsRemaining { get; set; }
        public Cinema Cinema { get; set; }
        public Movie Movie { get; set; }
        public Screening() { }
        public Screening(int n, DateTime d, string t, Cinema c, Movie m)
        {
            ScreeningNo = n;
            ScreeningDateTime = d;
            ScreeningType = t;
            Cinema = c;
            Movie = m;
        }
        public override string ToString()
        {
            return string.Format("Screening Number: {0} Screening Date Time: {1} Screening Type: {2} Cinema: {3} Movie: {4}", ScreeningNo, ScreeningDateTime, ScreeningType, Cinema.ToString(), Movie.ToString());
        }
        public int CompareTo(Screening s)
        {
            if (SeatsRemaining > s.SeatsRemaining)
                return 1;
            else if (SeatsRemaining == s.SeatsRemaining)
                return 0;
            else
                return -1;
        }
    }
    abstract class Ticket
    {
        public Screening Screening { get; set; }
        public Ticket() { }
        public Ticket(Screening s) { Screening = s; }
        abstract public double CalculatePrice();
        public override string ToString() { return string.Format("Screening: {}", Screening); }
    }
    class Order
    {
        public int OrderNo { get; set; }
        public DateTime OrderDateTime { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
        public List<Ticket> TicketList { get; set; }
        public Order() { }
        public Order(int n, DateTime d)
        {
            OrderNo = n;
            OrderDateTime = d;
        }
        public void AddTicket(Ticket t)
        {

        }
        public override string ToString() { return string.Format("Order Number: {} Order Date Time: {} Amount: {} Status: {} Ticket List: {}", OrderNo, OrderDateTime, Amount, Status, String.Join("/", TicketList)); }
    }
    class Student : Ticket
    {
        public string LevelOfStudy { get; set; }
        public Student() { }
        public Student(Screening s, string l)
        {
            Screening = s;
            LevelOfStudy = l;
        }
        public override double CalculatePrice()
        {
            double amount = 0;
            if (Screening.ScreeningDateTime.DayOfWeek == DayOfWeek.Friday && Screening.ScreeningDateTime.DayOfWeek == DayOfWeek.Saturday && Screening.ScreeningDateTime.DayOfWeek == DayOfWeek.Sunday)
            {
                if (Screening.ScreeningType == "2D") { amount = 12.5; }
                else if (Screening.ScreeningType == "3D") { amount = 14.0; }
            }
            else
            {
                if ((Screening.ScreeningDateTime - Screening.Movie.OpeningDate).Days < 7)
                {
                    if (Screening.ScreeningType == "2D") { amount = 8.5; }
                    else if (Screening.ScreeningType == "3D") { amount = 11.0; }
                }
                else
                {
                    if (Screening.ScreeningType == "2D") { amount = 7.0; }
                    else if (Screening.ScreeningType == "3D") { amount = 8.0; }
                }
            }
            return amount;
        }
        public override string ToString() { return base.ToString() + string.Format("Level Of Study: {}", LevelOfStudy); }
    }
    class SeniorCitizen : Ticket
    {
        public int YearOfBirth { get; set; }
        public SeniorCitizen() { }
        public SeniorCitizen(Screening s, int y)
        {
            Screening = s;
            YearOfBirth = y;
        }
        public override double CalculatePrice()
        {
            double amount = 0;
            if (Screening.ScreeningDateTime.DayOfWeek == DayOfWeek.Friday && Screening.ScreeningDateTime.DayOfWeek == DayOfWeek.Saturday && Screening.ScreeningDateTime.DayOfWeek == DayOfWeek.Sunday)
            {
                if (Screening.ScreeningType == "2D") { amount = 12.5; }
                else if (Screening.ScreeningType == "3D") { amount = 14.0; }
            }
            else
            {
                if ((Screening.ScreeningDateTime - Screening.Movie.OpeningDate).Days < 7)
                {
                    if (Screening.ScreeningType == "2D") { amount = 8.5; }
                    else if (Screening.ScreeningType == "3D") { amount = 11.0; }
                }
                else
                {
                    if (Screening.ScreeningType == "2D") { amount = 5.0; }
                    else if (Screening.ScreeningType == "3D") { amount = 6.0; }
                }
            }
            return amount;
        }
        public override string ToString() { return base.ToString() + string.Format("Year Of Birth: {}", YearOfBirth); }
    }
    class Adult : Ticket
    {
        public bool PopcornOffer { get; set; }
        public Adult() { }
        public Adult(Screening s, bool p)
        {
            Screening = s;
            PopcornOffer = p;
        }
        public override double CalculatePrice()
        {
            double amount = 0;
            if (Screening.ScreeningDateTime.DayOfWeek == DayOfWeek.Friday && Screening.ScreeningDateTime.DayOfWeek == DayOfWeek.Saturday && Screening.ScreeningDateTime.DayOfWeek == DayOfWeek.Sunday)
            {
                if (Screening.ScreeningType == "2D") { amount = 12.5; }
                else if (Screening.ScreeningType == "3D") { amount = 14.0; }
            }
            else
            {
                if (Screening.ScreeningType == "2D") { amount = 8.5; }
                else if (Screening.ScreeningType == "3D") { amount = 11.0; }
            }
            return amount;
        }
        public override string ToString() { return base.ToString() + string.Format("Popcorn Offer: {}", PopcornOffer); }
    }
}